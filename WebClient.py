import os
import sys
import time

from socket import *

requests = []

requests.append('POST /key/user content-length 6  admin')
requests.append('POST /key/pass content-length 4  pass')
requests.append('GET /key/user  ')
requests.append('GET /key/pass  ')
requests.append('GET /key/junk  ')
requests.append('POST /key/pass content-length 10  securepass')
requests.append('GET /key/pass  ')
requests.append('DELETE /key/pass  ')
requests.append('GET /key/pass  ')
requests.append('DELETE /key/junk  ')
requests.append('GET /file/hello.txt  ')
requests.append('GET /file/junk.txt  ')
requests.append('GET /file/secret.txt  ')
requests.append('GET /key/user userless-header True  ')

os.system('echo "Hello darkness my old friend..." > hello.txt')
os.system('echo "This is my dirty little secret!" > secret.txt')
os.system('chmod -r secret.txt')

server_name = '127.0.0.1'
server_port = int(sys.argv[1])

client_socket = socket(AF_INET, SOCK_STREAM)
client_socket.connect((server_name, server_port))

for request in requests[2:]:

    print(request)
    client_socket.send(request.encode())
    response = client_socket.recv(4096)
    print(response.decode())

split_request = 'GET /key/user  '
client_socket.send(split_request[:6].encode())
time.sleep(1)
client_socket.send(split_request[6:].encode())
response = client_socket.recv(4096)

split_request_2 = 'POST /key/user content-length 11  jethro kuan'
client_socket.send(split_request_2[:-5].encode())
time.sleep(1)
client_socket.send(split_request_2[-5:].encode())
response = client_socket.recv(4096)


client_socket.close()

os.system('rm hello.txt')
os.system('rm secret.txt')
