import socket
import os
import sys

TCP_IP = "127.0.0.1"
BUFFER_SIZE = 1000

class DataStore(object):
    def __init__(self):
        self.store = {}

    def exists(self, key):
        return key in self.store

    def set(self, key, val):
        self.store[key] = val

    def get(self, key):
        return self.store.get(key)

    def delete(self, key):
        if self.exists(key):
            data = self.store.get(key)
            del self.store[key]
            return data
        return None


class HTTPRequest(object):
    @staticmethod
    def parse_request(s):
        """Parses a HTTP-like request string, and outputs a request object.

        Args:
            s (string): HTTP-like request bytestring

        Returns:
            request (dict): HTTP request
        """
        request = {}
        end_of_req = None
        separation_idx = s.find(b"  ")
        if separation_idx == -1:
            return request, end_of_req

        headers = s[:separation_idx].decode("ascii").split() # This part is ascii string
        request['method'] = headers[0].upper() #  First item is method (e.g. GET)
        request['path'] = headers[1]           # Second item is case-sensitive path (e.g. /key/1)

        # Rest is header key value pairs
        for idx in range(2, len(headers), 2):
            k = headers[idx].lower()
            v = headers[idx+1]
            request[k] = v

        if 'content-length' in request: # if there's content, truncate to content-length
            start = separation_idx + 2
            length = int(request['content-length'])
            end = start + length

            if len(s) < end:
                request = None
                end_of_req = None
            else:
                request['body'] = s[start:end]
                end_of_req = end - 1
        else:
            end_of_req = separation_idx + 1

        return request, end_of_req


class HTTPResponse(object):
    @staticmethod
    def construct_response(response):
        """Constructs a byte response in the HTTP-like protocol.

        Args:
            response (dict): Dictionary containing required information.

        Returns:
            (bytes) bytestring containing response
        """
        status = HTTPResponse.status_to_string(response.get("status")).encode()
        headers = HTTPResponse.headers_to_string(response).encode()
        body = response.get("body", b"")
        response_str = status
        if headers:
            response_str += b" "
            response_str += headers

        response_str += b"  "

        if body:
            response_str += body
        return response_str

    @staticmethod
    def status_to_string(status):
        status_map = {
            404: "404 NotFound",
            200: "200 OK",
            400: "400 BadRequest",  # For when content-body length mismatch
            403: "403 Forbidden",
        }

        return status_map.get(status, "{} Foo".format(status))

    @staticmethod
    def headers_to_string(response):
        response_headers = {
            k: v
            for k, v in response.items() if k not in ["status", "body"]
        }
        return " ".join(
            ["{} {}".format(k, v) for k, v in response_headers.items()])


class WebServer(object):
    def __init__(self, port):
        self.port = port
        self.store = DataStore()

    def start(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((TCP_IP, self.port))
        self.socket.listen(1)

        while True:
            try:
                conn, addr = self.socket.accept()
                print("Connection Address:", addr)
                self.handle_client_connection(conn)
                print("Connection ending:", addr)
            except (KeyboardInterrupt, SystemExit):
                break

        self.shutdown()

    def handle_client_connection(self, conn):
        data = b""
        receiving_data = True

        while True:
            if receiving_data:
                chunk = conn.recv(BUFFER_SIZE)
                if not chunk:
                    receiving_data = False
                else:
                    data += chunk

            if not receiving_data and not data:
                return

            while data:
                # Each time data is received, attempt a parse
                request, eor = HTTPRequest.parse_request(data)
                if not request:
                    break

                print("Got request: ", request)
                response = self.handle_request(request)
                response_s = HTTPResponse.construct_response(response)
                print("Responding with: ", response_s)
                conn.sendall(response_s)

                data = data[eor+1:]


    def handle_request(self, request):
        path = request["path"]
        if path[:5] == "/key/":
            request["path"] = path[5:]
            return self.handle_key_request(request)
        elif request["path"][:6] == "/file/":
            request["path"] = path[6:]
            return self.handle_file_request(request)
        else:
            # Invalid HTTP Request
            return {"status": 400}

    def handle_file_request(self, request):
        """Handle file HTTP requests."""
        if not request["method"] == "GET":
            return {"status": 400}
        if os.path.isfile(request["path"]):
            try:
                with open(request["path"], "wb") as output_file:
                    body = output_file.read()
                return {
                    "status": 200,
                    "content-length": len(body),
                    "body": body
                }
            except:
                return {"status": 403}
        else:
            return {"status": 404}

    def handle_key_request(self, request):
        if request["method"] == "GET":
            result = self.store.get(request["path"])

            if result is None:
                return {"status": 404}

            return {
                "status": 200,
                "content-length": len(result),
                "body": result
            }
        elif request["method"] == "POST":
            if request.get("content-length") and int(
                    request.get("content-length")) == len(request.get("body")):
                self.store.set(request["path"], request["body"])
                return {"status": 200}
            else:
                return {"status": 400}

        elif request["method"] == "DELETE":
            result = self.store.delete(request["path"])
            if result is not None:
                return {
                    "status": 200,
                    "content-length": len(result),
                    "body": result
                }
            else:
                return {"status": 404}

        else:
            return {"status": 400}

    def shutdown(self):
        """Closes the TCP connection."""
        print("Closing TCP connection...")
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        print("Connection closed. Exiting.")
        exit(0)


if __name__ == "__main__":
    port = int(sys.argv[1])
    server = WebServer(port)
    server.start()
