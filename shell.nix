with import <nixpkgs> {};

pkgs.mkShell {
  buildInputs = [
    python3
    netcat-gnu
  ] ++ (with python37Packages; [
    yapf
    python-language-server
  ]);
}
